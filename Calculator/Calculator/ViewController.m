//
//  ViewController.m
//  Calculator
//
//  Created by Paweł Sternik on 13.04.2015.
//  Copyright (c) 2015 Paweł Sternik. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@end

@implementation ViewController {
    NSMutableArray *tableData;
    BOOL flagsOperation;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    tableData = [[NSMutableArray alloc]init];
    self.labelText.text = @"";
    self.labelTextOperate.text = @"";
    flagsOperation = false;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)number:(id)sender {
    if(flagsOperation) {
        self.labelTextOperate.text = @"";
        flagsOperation = false;
    }
    NSString *number = [sender currentTitle];
    self.labelText.text = [self.labelText.text stringByAppendingString:number];
}


- (IBAction)actionWithOperation:(id)sender {
    // Pobieram pierwsza liczbe
    NSString *temp = [[NSString alloc]initWithString:self.labelText.text];
    float newNumber = [temp floatValue];
    self.labelTextOperate.text = [self.labelTextOperate.text stringByAppendingString:temp];
    NSNumber *number= [NSNumber numberWithFloat:newNumber];
    [tableData addObject:number];
    
    // Ustawiam operacje
    NSString *action = [sender currentTitle];
    [tableData addObject:action];
    self.labelTextOperate.text = [self.labelTextOperate.text stringByAppendingString:action];
    self.labelText.text = @"";
}


- (IBAction)finish:(id)sender {
    // Pobieram druga liczbe
    NSString *temp = [[NSString alloc]initWithString:self.labelText.text];
    self.labelTextOperate.text = [self.labelTextOperate.text stringByAppendingString:temp];
    float newNumber = [temp floatValue];
    NSNumber *number= [NSNumber numberWithFloat:newNumber];
    [tableData addObject:number];
    
    NSNumber *result;
    NSString *action = tableData[1];
    NSNumber *numberOne = tableData[0];
    NSNumber *numberTwo = tableData[2];
    
    if([action isEqualToString:@"+"]) {
        result = @([numberOne floatValue]+[numberTwo floatValue]);
    }
    else if([action isEqualToString:@"-"]) {
        result = @([numberOne floatValue]-[numberTwo floatValue]);
    }
    else if([action isEqualToString:@"x"]) {
        result = @([numberOne floatValue]*[numberTwo floatValue]);
    }
    else if([action isEqualToString:@"/"]) {
        result = @([numberOne floatValue]/[numberTwo floatValue]);
    }
    NSString *resultText = [result stringValue];
    self.labelText.text = resultText;
    self.labelTextOperate.text = [self.labelTextOperate.text stringByAppendingFormat:@" = "];
    self.labelTextOperate.text = [self.labelTextOperate.text stringByAppendingString:resultText];
    
    // Czyszczenie tablicy
    [tableData removeAllObjects];
    flagsOperation = true;
    self.labelText.text = @"";
}

- (IBAction)clearOperation:(id)sender {
    flagsOperation = false;
    self.labelText.text = @"";
    self.labelTextOperate.text = @"";
    [tableData removeAllObjects];
}

@end
