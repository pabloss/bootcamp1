//
//  AppDelegate.h
//  Calculator
//
//  Created by Paweł Sternik on 13.04.2015.
//  Copyright (c) 2015 Paweł Sternik. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

