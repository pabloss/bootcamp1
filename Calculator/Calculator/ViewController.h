//
//  ViewController.h
//  Calculator
//
//  Created by Paweł Sternik on 13.04.2015.
//  Copyright (c) 2015 Paweł Sternik. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ViewController : UIViewController

@property(weak,nonatomic) IBOutlet UILabel *labelText;
@property(weak,nonatomic) IBOutlet UILabel *labelTextOperate;

- (IBAction)number:(id)sender;
- (IBAction)actionWithOperation:(id)sender;
-(IBAction)finish:(id)sender;
- (IBAction)clearOperation:(id)sender;
@end

